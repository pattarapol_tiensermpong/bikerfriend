package com.example.midas.bikerfriend;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Midas on 29/8/2558.
 */
public class finishCyclingController extends Activity implements OnMapReadyCallback,GoogleApiResponse{
    private String BROWSER_API_KEY = "AIzaSyA7H8qTQvAL4tugGe5VUZ_cxULpWumrDlY";
    private TextView tmp;
    private GoogleMap routeMap;
    private ArrayList<LatLng> routePoints;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish);
        tmp = (TextView) findViewById(R.id.tmpText);
        Intent intent = getIntent();
        routePoints = intent.getParcelableArrayListExtra("routePoint");
        tmp.setText("" + routePoints.size());

        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        routeMap = mapFragment.getMap();
        String url = "https://roads.googleapis.com/v1/snapToRoads?path=";
        for(int i =0;i < routePoints.size();i++){
            url += ""+routePoints.get(i).latitude+","+routePoints.get(i).longitude;
            if (i != routePoints.size()-1){
                url += "|";
            }
        }
        url += "&key="+BROWSER_API_KEY;
        makeConnection(url);
    }

    public void makeConnection(String url) {
        ConnectGoogleAPIClass client = new ConnectGoogleAPIClass();
        client.delegate = this;
        client.execute(url);
    }
    @Override
    public void onMapReady(GoogleMap map) {
        //routeMap = map;
        drawRoute();
    }

    @Override
    public void postResult(String asyncresult) {
        try {
            JSONObject reader = new JSONObject(asyncresult);
            JSONArray snappedPoint  = reader.getJSONArray("snappedPoints");
            routePoints = new ArrayList<>();
            for(int i=0;i<snappedPoint.length();i++) {
                JSONObject object = snappedPoint.getJSONObject(i);
                JSONObject location = object.getJSONObject("location");
                double latitude = location.getDouble("latitude");
                double longtitude = location.getDouble("longitude");
                LatLng loc = new LatLng(latitude,longtitude);
                routePoints.add(loc);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void drawRoute(){
        PolylineOptions pOptions = new PolylineOptions()
                .width(5)
                .color(Color.BLUE)
                .geodesic(true);
        for (int z = 0; z < routePoints.size(); z++) {
            LatLng point = routePoints.get(z);
            pOptions.add(point);
        }
        routeMap.addPolyline(pOptions);
    }
}
