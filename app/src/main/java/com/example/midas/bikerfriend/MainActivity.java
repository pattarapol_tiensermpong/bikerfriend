package com.example.midas.bikerfriend;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.android.gms.common.*;
import com.google.android.gms.common.api.*;
import com.google.android.gms.common.api.GoogleApiClient.*;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener{

    private ToggleButton startButton;
    private Button goManageTripButton;
    private TextView timeText,locationText;
    private Handler myHandler = new Handler();
    private long timeInMillies = 0L , timeSwap = 0L, finalTime = 0L, startTime = 0L;
    private GoogleApiClient mGoogleApiClient;
    private Location lastLocation,currentLocation ;
    private boolean isRequestUpdate = false;
    private LocationRequest mLocationRequest;
    private ArrayList<LatLng> routePoints;
    protected final static String REQUESTING_LOCATION_UPDATES_KEY = "requesting-location-updates-key";
    protected final static String LOCATION_KEY = "location-key";
    //protected final static String LAST_UPDATED_TIME_STRING_KEY = "last-updated-time-string-key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buildGoogleApiClient();
        createLocationRequest();
        init();
        // Update values using data stored in the Bundle.
        updateValuesFromBundle(savedInstanceState);
    }

    private void init() {
        timeText = (TextView) findViewById(R.id.timeTextView);
        locationText = (TextView) findViewById(R.id.locationTxt);
        startButton = (ToggleButton) findViewById(R.id.startButton);
        routePoints = new ArrayList<>();
        startButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    startCycling();
                } else {
                    stopCycling();
                }
            }
        });
        goManageTripButton = (Button) findViewById(R.id.manageTrip);
    }

    private void startCycling(){
        startTime = SystemClock.uptimeMillis();
        myHandler.postDelayed(startTimer, 0);
        isRequestUpdate = true;
        startLocationUpdates();
    }

    private void stopCycling(){
        startTime = 0L;
        myHandler.removeCallbacks(startTimer);
        isRequestUpdate = false;
        stopLocationUpdates();
        locationText.setText("Location save : " + routePoints.size());
        Intent intent = new Intent(this, finishCyclingController.class);
        intent.putParcelableArrayListExtra("routePoint", routePoints);
        startActivity(intent);
        routePoints.clear();
    }

    private Runnable startTimer = new Runnable() {

        public void run() {
            timeInMillies = SystemClock.uptimeMillis() - startTime;
            finalTime = timeSwap + timeInMillies;

            int seconds = (int) (finalTime / 1000);
            int minutes = seconds / 60;
            seconds = seconds % 60;
            updateTimeUI(minutes,seconds);
        }

    };

    private void updateTimeUI(int minutes,int seconds) {
        timeText.setText("" + String.format("%02d", minutes/60) + ":" + String.format("%02d",minutes) + ":"
                + String.format("%02d", seconds));
        myHandler.postDelayed(startTimer, 0);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }
    private void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }
    private void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;
        if (currentLocation != null) {
            double latitude = currentLocation.getLatitude();
            double longitude = currentLocation.getLongitude();
            LatLng latLng = new LatLng(latitude, longitude);
            if(routePoints.size() == 0 || (routePoints.size() > 0 && SphericalUtil.computeDistanceBetween(routePoints.get(routePoints.size() - 1), latLng) > 20)) {
                /*for (int z = 0; z < routePoints.size(); z++) {
                    LatLng point = routePoints.get(z);
                    pOptions.add(point);
                }*/
                //line = MyMap.addPolyline(pOptions);
                routePoints.add(latLng);
            }
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        lastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
    }

    @Override
     protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }


    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(REQUESTING_LOCATION_UPDATES_KEY,
                isRequestUpdate);
        savedInstanceState.putParcelable(LOCATION_KEY, currentLocation);
        //savedInstanceState.putString(LAST_UPDATED_TIME_STRING_KEY, mLastUpdateTime);
        super.onSaveInstanceState(savedInstanceState);
    }

    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            // Update the value of mRequestingLocationUpdates from the Bundle, and
            // make sure that the Start Updates and Stop Updates buttons are
            // correctly enabled or disabled.
            if (savedInstanceState.keySet().contains(REQUESTING_LOCATION_UPDATES_KEY)) {
                isRequestUpdate = savedInstanceState.getBoolean(
                        REQUESTING_LOCATION_UPDATES_KEY);
            }

            // Update the value of mCurrentLocation from the Bundle and update the
            // UI to show the correct latitude and longitude.
            if (savedInstanceState.keySet().contains(LOCATION_KEY)) {
                // Since LOCATION_KEY was found in the Bundle, we can be sure that
                // mCurrentLocationis not null.
                currentLocation = savedInstanceState.getParcelable(LOCATION_KEY);
            }
            /*
            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains(LAST_UPDATED_TIME_STRING_KEY)) {
                mLastUpdateTime = savedInstanceState.getString(
                        LAST_UPDATED_TIME_STRING_KEY);
            }*/
        }
    }
}
